###### *Module 2 : Les logiciels*	
###### *Séquence n°2: Le traitement de texte*	
###### *Niveau : Tronc commun*
###### *Type : TP*
###### *Séance 3: La mise en forme et la mise en page*

**Thématique :** Traitement de texte
  
**Notions liées :** Elaboration d’un document : 
- Logiciel 
- Saisie
- Mise en forme 
- Insertion d’image 
- Mise en page

**Résumé de l’activité :** L’activité consiste à la réalisation d’un TP sur ordinateur plus particulièrement sur un logiciel de traitement de texte, ou les apprenants seront amenés à mettre en forme le texte et les paragraphes de la page.

**Objectifs :** être capable d’exploiter efficacement un logiciel de traitement de textes. 
- Saisir un texte 
- Mettre en forme les caractères et les paragraphes
- Mettre en forme la page

**Auteur :** Majdouline AIT

**Durée de l’activité :** 2h

**Forme de participation :** individuelle ou en binôme, en autonomie.

**Matériel nécessaire :** Ordinateur et un logiciel de traitement de texte.

**Préparation :** Aucune 

**Autres références :**

**Fiche élève cours :**

**Fiche élève activité :**  


