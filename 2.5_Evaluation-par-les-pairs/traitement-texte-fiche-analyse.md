# Mise en œuvre de la séance
### Fiche élève activité :
### Fiche prof :

**Etape 0 :**  
Allumer les ordinateurs puis accéder à un logiciel de traitement de texte (dans notre cas on va utiliser Word) 

**Etape 1 : exercice1**  
Les apprenants vont commencer par la réalisation de l’exercice1, cette activité peut se faire individuellement si les ressources le permettent, sinon en binôme.  
Cet exercice a pour but d’initier les apprenants aux fonctionnalités générales et à l’usage du logiciel et l’accommodation avec l’interface graphique.  
Il consiste majoritairement à l’exécution des instructions de base de traitement du texte.  
On observe si il y a des blocages chez les apprenants et on fait on sorte qu’ils les dépassent tous pour pouvoir suivre correctement les activités qui vont suivre.  
La durée estimée de l’exercice1 est de 25min.  

**Etape 2 : exercice2**  
C’est durant cet exercice que les apprenants vont mettre en pratique toutes les notions qu’ils ont assimilé dans le cours dans une situation concrète. Commençant par la saisie du texte, la mise en page et mise en forme jusqu’à l’enregistrement et la présentation du résultat final.  
L’exercice2 consiste à rédiger, mettre en forme et mettre en page un article sur le réseau social Instagram, connu par la totalité des apprenants. 
Cette activité se fait aussi en individuelle si possible, et en binôme dans le cas contraire. 
Dans cette activité les apprenants seront livrés à eux même, pour essayer au maximum de se débrouiller tous seul, et apporter leur touche personnelle au travail réalisé. 

On observe chez les apprenants les éléments suivants:
- Compréhension de l’activité et des consignes.
- Maitrise de l’environnement de travail.
- Se tenir à ce qui est demandé dans les instructions
- Présentation du travail réalisé.

La durée estimée de l’exercice2 est de 1h30min

**Etape 3 :**   
Prendre du recul sur le déroulement de la séance, relever les éléments qui ont suscité le plus de difficulté auprès des apprenants et les obstacles qui y ont contribué. 


