# Partie 1
## sous partie 1: texte
Une phrase sans rien  
*Une phrase en italique.*  
**Une pharse en gras.**  
Un lien vers [fun-mooc.fr](https://lms.fun-mooc.fr)  
Une ligne de `code`  
## sous partie 2: listes
**liste a puce**  
- item  
  - item1  
  - item2  
- item  
- item    

**liste numéroté**  
1. item  
2. item  
3. item  
## sous partie 3: codes

``` 
# extrait de code
```
